import { createRoot } from 'react-dom/client'
import React, { RefObject, useRef, useState } from 'react'
import { Canvas, MeshProps, useFrame } from '@react-three/fiber'
import { BufferGeometry, Material, Mesh } from 'three'

const Plane = (props: MeshProps) => {
    // This reference gives us direct access to the THREE.Mesh object
    const ref = useRef<RefObject<Mesh<BufferGeometry, Material | Material[]>>>()

    return (
        <mesh
            {...props}
            // @ts-ignore
            ref={ref}
            scale={[Infinity, Infinity, Infinity]}
            rotation={[-Math.PI / 2, 0, 0]}
            position={[0, -0.5, 0]}
            receiveShadow
        >
            <planeGeometry />
            <meshStandardMaterial color={'pink'} />
        </mesh>
    )
}

export default Plane;
