import React, { useRef, useState } from 'react'

const SceneLights = () => {
    return (
        <>
            {/* <ambientLight intensity={1} /> */}
            <directionalLight
                castShadow
                position={[2.5, 8, 5]}
                intensity={2}
                shadow-mapSize-width={1024}
                shadow-mapSize-height={1024}
                shadow-camera-far={50}
                shadow-camera-left={-10}
                shadow-camera-right={10}
                shadow-camera-top={10}
                shadow-camera-bottom={-10}
            />
            {/* <pointLight castShadow position={[10, 0, 20]} intensity={1} />
            <pointLight castShadow position={[0, 10, 0]} intensity={0.5} /> */}
        </>
    )
}

export default SceneLights;
