import React, { Ref, useRef, useState } from 'react'
import { MeshProps, useFrame } from '@react-three/fiber'
import { RoundedBox } from '@react-three/drei'
import { BufferGeometry, Material, Mesh } from 'three'

const Box = (props: MeshProps) => {
    const [randomOffset] = useState(Math.random() * Math.PI)

    // This reference gives us direct access to the THREE.Mesh object
    const ref = useRef<Ref<Mesh<BufferGeometry, Material | Material[]>> | null>(null)

    // Hold state for hovered and clicked events
    const [hovered, hover] = useState(false)

    // Subscribe this component to the render-loop, rotate the mesh every frame
    // useFrame((state, delta) => (ref.current.rotation.x += 0.01))

    // use useframe hook to animate the y axis of the box
    useFrame((state, delta) => {
        if (!ref.current) return;
        // @ts-ignore
        ref.current.position.y += Math.sin((randomOffset + state.clock.elapsedTime)) / 500
    })


    return (
        <RoundedBox
            {...props}
            // @ts-ignore
            ref={ref}
            onPointerOver={() => hover(true)}
            onPointerOut={() => hover(false)}
            receiveShadow
            castShadow
        >
            <meshStandardMaterial metalness={0.9} roughness={0} attach='material' color={hovered ? 'hotpink' : 'DeepSkyBlue'} />
            {/* <meshPhongMaterial a attach='material' color={hovered ? 'hotpink' : 'orange'} /> */}
        </RoundedBox>
    )
}

export default Box;
