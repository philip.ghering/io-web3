import React, { RefObject, useRef, useState } from 'react'
import { MeshProps, useFrame } from '@react-three/fiber'
import { BufferGeometry, Material, Mesh } from 'three'

const Cylinder = (props: MeshProps) => {
    // This reference gives us direct access to the THREE.Mesh object
    const ref = useRef<RefObject<Mesh<BufferGeometry, Material | Material[]>>>()

    // Hold state for hovered and clicked events
    const [hovered, hover] = useState(false)

    // Subscribe this component to the render-loop, rotate the mesh every frame
    // useFrame((state, delta) => (ref.current.rotation.x += 0.01))

    // Return the view, these are regular Threejs elements expressed in JSX
    return (
        <mesh
            {...props}
            // @ts-ignore
            ref={ref}
            onPointerOver={(event) => hover(true)}
            onPointerOut={(event) => hover(false)}
            receiveShadow
            castShadow
        >
            {/* CylinderGeometry(radiusTop : Float, radiusBottom : Float, height : Float, radialSegments : Integer, heightSegments : Integer, openEnded : Boolean, thetaStart : Float, thetaLength : Float) */}
            <cylinderGeometry args={[0.5, 0.5, 1, 20]} />
            <meshStandardMaterial color={hovered ? 'hotpink' : 'orange'} />
        </mesh>
    )
}

export default Cylinder;
