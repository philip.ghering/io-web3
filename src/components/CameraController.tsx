import React, { FunctionComponent, useEffect, useMemo, useRef, useState } from 'react'
import { useFrame, useThree } from '@react-three/fiber'
import CameraControls from 'camera-controls'
import { degreeToRadian } from '../utils/utils'

interface CameraControllerProps {
    playerPos: number[]
}

const DIST = 4
const HEIGHT = 4

const CameraController: FunctionComponent<CameraControllerProps> = ({ playerPos }) => {
    const camera = useThree((state) => state.camera)
    const gl = useThree((state) => state.gl)
    const controls = useMemo(() => new CameraControls(camera, gl.domElement), [])

    useEffect(() => {
        controls.setLookAt(
            playerPos[0] - DIST,
            HEIGHT,
            playerPos[2] + DIST,
            0,
            0,
            0,
            true
        )

        controls.maxDistance = 20
        controls.maxPolarAngle = degreeToRadian(80)
    }, [])

    return useFrame((state, delta) => {
        return controls.update(delta)
    })
}

export default CameraController;
