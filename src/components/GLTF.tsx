import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { useFrame, useLoader } from '@react-three/fiber'
import { Clone } from '@react-three/drei'
import { Euler, Group, Vector3 } from 'three'
import { useEffect, useRef, useState } from 'react'
import { shapeAnimationSettings } from '../utils/utils'

const GLTF = ({
    type,
    position,
    animate = true,
    animationIntensity = 500,
    animationYOffset = 0,
    scale = 1,
}: {
    type: 'cube' | 'quarter-circle' | 'cylinder' | 'floor',
    position: Vector3
    animate?: boolean
    animationIntensity?: number
    animationYOffset?: number
    scale?: number
}) => {
    const ref = useRef<Group | null>(null)

    const [{ animationOffset, randomRotation }] = useState(animate
        ? shapeAnimationSettings()
        : { animationOffset: 0, randomRotation: new Euler(0, 0, 0) }
    )

    useEffect(() => {
        if (!ref.current?.position?.y) return;
        ref.current.position.y = animationYOffset
    }, [animationIntensity, animationYOffset])

    const { scene: cubeScene } = useLoader(GLTFLoader, '/Cube.gltf')
    const { scene: quarterCubeScene } = useLoader(GLTFLoader, '/quarter-circle.gltf')
    const { scene: cylinderScene } = useLoader(GLTFLoader, '/Cylinder.gltf')
    const { scene: tileScene } = useLoader(GLTFLoader, '/Tile.gltf')

    useFrame((state, delta) => {
        if (!ref.current || !animate) return;
        // @ts-ignore
        ref.current.position.y +=
            Math.sin((animationOffset + state.clock.elapsedTime))
            * animationIntensity;
    })

    const getType = (type: 'cube' | 'quarter-circle' | 'cylinder' | 'floor') => {
        switch (type) {
            case 'cube':
                return cubeScene
            case 'quarter-circle':
                return quarterCubeScene
            case 'cylinder':
                return cylinderScene
            case 'floor':
                return tileScene
        }
    }

    return (
        <Clone
            ref={ref}
            scale={scale}
            object={getType(type)}
            position={position}
            rotation={randomRotation}
            receiveShadow
            castShadow
        />
    )
};

export default GLTF;
