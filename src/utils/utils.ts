import { Euler, Vector3 } from "three";
import { IntRange } from "./types";

export const degreeToRadian = (degree: number) => {
    return degree * Math.PI / 180
}

export const types = ['cube', 'quarter-circle', 'cylinder'] as const;

export const makeGrid = (
    gridSize: number,
    gridDensity: number = 0.7,
) => (
    new Array(gridSize * gridSize).fill(0).map((_, i) => {
        const x = (i % gridSize) - gridSize / 2;
        const z = Math.floor(i / gridSize) - gridSize / 2;
        const y = Math.random() * 0.2;

        const shouldGetBlock = Math.random() > (1 - gridDensity);

        return {
            id: i,
            position: new Vector3(x, y, z),
            floorPos: new Vector3(x, 0, z),
            type: shouldGetBlock ? types[Math.floor(Math.random() * 3)] : undefined,
        }
    })
);

export const shapeAnimationSettings = () => {
    const animationOffset = Math.random() * Math.PI;

    const randomRotation = new Euler(
        0,
        degreeToRadian(Math.floor(Math.random() * 4) * 90),
        0,
        'XYZ',
    );

    return {
        animationOffset,
        randomRotation,
    }
}