import { KeyboardControls, Environment } from '@react-three/drei'
import { Canvas, useThree } from '@react-three/fiber'
import * as THREE from 'three'
import CameraControls from 'camera-controls'
import { useControls } from 'leva'

import Plane from './components/Plane'
import SceneLights from './components/SceneLights'
import CameraController from './components/CameraController'
import GLTF from './components/GLTF'
import { Fragment, useMemo } from 'react'
import { makeGrid } from './utils/utils'

CameraControls.install({ THREE: THREE });

const POS = [0, 0, 0];
const posVector = new THREE.Vector3(...POS);

const App = () => {
  const { gridSize, gridDensity } = useControls({
    gridSize: { value: 40, step: 2, min: 10, max: 60 },
    gridDensity: { value: 0.7, step: 0.01, min: 0, max: 1 },
  })

  const grid = useMemo(() => makeGrid(gridSize, gridDensity), [gridSize, gridDensity]);

  const { animateBlocks, animationIntensity, animationYOffset } = useControls({
    animateBlocks: true,
    animationIntensity: { value: 0.0001, step: 0.00001, min: 0.000001, max: 0.01 },
    animationYOffset: { value: 0, step: 0.01, min: -2, max: 10 },
  })

  return (
    <KeyboardControls
      map={[
        { name: 'forward', keys: ['ArrowUp', 'w', 'W'] },
        { name: 'backward', keys: ['ArrowDown', 's', 'S'] },
        { name: 'leftward', keys: ['ArrowLeft', 'a', 'A'] },
        { name: 'rightward', keys: ['ArrowRight', 'd', 'D'] },
        { name: 'jump', keys: ['Space'] },
      ]}
    >
      <Canvas
        style={{ height: '100vh' }}
        shadows
        camera={{ position: posVector }}
      >
        <SceneLights />

        {/* <Environment
          background={false} // can be true, false or "only" (which only sets the background) (default: false)
          // blur={0.1} // blur factor between 0 and 1 (default: 0, only works with three 0.146 and up)
          // files={['px.png', 'nx.png', 'py.png', 'ny.png', 'pz.png', 'nz.png']}
          // path="/venice_sunset_1k.hdr"
          preset="city"
          scene={undefined} // adds the ability to pass a custom THREE.Scene, can also be a ref
          encoding={undefined} // adds the ability to pass a custom THREE.TextureEncoding (default: THREE.sRGBEncoding for an array of files and THREE.LinearEncoding for a single texture)
        /> */}

        <CameraController playerPos={POS} />

        {grid.map(({ id, position, floorPos, type }) => (
          <Fragment key={id}>
            {type && (
              <GLTF
                position={position}
                type={type}
                animate={animateBlocks}
                animationIntensity={animationIntensity}
                animationYOffset={animationYOffset}
                scale={0.95}
              />
            )}
            <GLTF
              position={floorPos}
              type={'floor'}
              animate={false}
              scale={0.999}
            />
          </Fragment>
        ))}

        <fog attach="fog" color="white" near={10} far={40} />
        <Plane />

        <axesHelper />
      </Canvas>
      HI
    </KeyboardControls>
  )
}

export default App
